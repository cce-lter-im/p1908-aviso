
% To call from the command line from this directory
% octave -q plot_lat15.m ../../Wrk ../../Plots 20190720

datadir  = argv(){1}  % directory with data file needed for this plot
plotdir  = argv(){2}  % output directory for saving plot
plotdate = argv(){3}  % date to plot YYYYMMDD

load Cali_coords.mat
load Cali_main_solid.mat

load([datadir '/' plotdate '_dt_product_d0.mat'])

pcolor(lonvv,latvv,latf15); 
shading flat;
axis([-124 -117 32 37]);
set(gca,'DataAspectRatio',[1 cosd(35.08) 1]);
hold on;
plot(cali_lon,cali_lat,'r','linewidth',0.5);
set(gca,'fontsize',15); 
xlabel('Longitude','fontsize',18);
ylabel('Latitude','fontsize',18); 
h = colorbar; 
caxis([30 39]);
h = colorbar; 
set(h,'fontsize',15);
ylabel(h,'Lat position 15 days ago','fontsize',18);
title(['Lat 15 ' plotdate],'fontsize',20);
set(gca,'tickdir','out');

patch(cali_main_lon,cali_main_lat,'k');

print(gcf, [plotdir '/' plotdate '_LAT15.png'], '-dpng', '-r300');

clear
exit
