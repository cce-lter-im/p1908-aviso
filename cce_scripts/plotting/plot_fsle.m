
% To call from the command line from this directory
% octave -q plot_fsle.m ../../Wrk ../../Plots 20190720

datadir  = argv(){1}  % directory with data file needed for this plot
plotdir  = argv(){2}  % output directory for saving plot
plotdate = argv(){3}  % date to plot YYYYMMDD

load Cali_coords.mat
load Cali_main_solid.mat

load([datadir '/' plotdate '_dt_product_d0_lambda_only.mat'])

fsle = -60.*60.*24.*lambda;
pcolor(lonvv,latvv,fsle); 
shading flat;
axis([-124 -117 32 37]);
set(gca,'DataAspectRatio',[1 cosd(35.08) 1]);
hold on;
plot(cali_lon,cali_lat,'r','linewidth',0.5);
set(gca,'fontsize',15); xlabel('Longitude','fontsize',18);
ylabel('Latitude','fontsize',18); 
h = colorbar; 
caxis([0 0.5]);
h = colorbar; 
set(h,'fontsize',15); 
caxis([0 0.5]);

ylabel(h,'FSLE (day^-^1)','fontsize',18);
title(['FSLE for ' plotdate],'fontsize',20);
set(gca,'tickdir','out');

patch(cali_main_lon,cali_main_lat,'k');

print(gcf, [plotdir '/' plotdate '_FSLE.png'], '-dpng', '-r300');

clear
exit