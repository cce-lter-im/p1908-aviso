
% To call from the command line from this directory
% octave -q plot_lat15.m ../../Data/uv ../../Plots 20190720

datadir  = argv(){1}  % directory with data file needed for this plot
plotdir  = argv(){2}  % output directory for saving plot
plotdate = argv(){3}  % date to plot YYYYMMDD

load Cali_coords.mat
load Cali_main_solid.mat

pkg load netcdf

fname = [datadir '/' 'nrt_global_allsat_phy_l4_' plotdate '_' plotdate '.nc']

lon = ncread(fname,'longitude');
lat = ncread(fname,'latitude');
ssh = ncread(fname,'adt');
u = ncread(fname,'ugos');
v = ncread(fname,'vgos');

imagesc(lon,lat,ssh'); 
set(gca,'Ydir','normal');
hold on;
quiver(lon,lat,u',v',0,'w','linewidth', 1);
axis([-124+360 -117+360 32 37]);
set(gca,'DataAspectRatio',[1 cosd(35.08) 1]);
hold on;
plot(cali_lon+360,cali_lat,'r','linewidth',0.5);
set(gca,'fontsize',15); 
xlabel('Longitude','fontsize',18);
ylabel('Latitude','fontsize',18); 
h = colorbar; 
caxis([0 0.5]);
h = colorbar; 
set(h,'fontsize',15); 
caxis([0.4 0.8]);
ylabel(h,'SSH (m)','fontsize',18);
title(['SSH for ' plotdate],'fontsize',20);
set(gca,'tickdir','out');

patch(cali_main_lon+360,cali_main_lat,'k');
quiver(241,35.5,0.5,0,0,'r','linewidth',2);


print(gcf, [plotdir '/' plotdate '_SSH.png'], '-dpng', '-r300');

clear
exit

