% Set zoom on the domain
% STRASSE campaign within:
%  -40W -32W
%   25N  30N
%Lon=[-50 -22]; 
%Lat=[15 40];

% OSCAHR campaign   
%Lon=[4 9.5]; 
%Lat=[41 45]; 
%fig_limits=[5 9.5 42 44];  

% Uncomment these lines to test over Mediterranean
%Lon=[-10 40]; 
%Lat=[20 60];   

%P1208 CCE-LTER
Lon=[-135 -115];
%Lat=[30 40];
Lat=[25 40];
% OUTPACE cruise
% OUTPACE cruise
%Lon=[150 210];
%Lat=[-30 -10];
fig_limits=[Lon(1) Lon(2) Lat(1) Lat(2)];
