function all_lagrangian_figs(fname,product,out_path,ctday)
% Create the figures using the all_lagrangian_diags .mat file
% Figures : FSLE, Okubo-Weiss parameter (OW), Lon/Lat advection, velocity
% Modified from:
% $DATA/KEOPS2/octave/keops/analysis/realtime_cron/all_lagrangianfigs_keops2.m
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 27/11/14 AD: added code to plot zooms around LD stations for all parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%---------------------------------------------------------
% % Comment function line and uncomment below to test
% fname='/home/OUTPACE/Wrk/20141128_nrt_global_2014_d6.mat'; %%%put here the name of the file produced by MAIN_Lagrangian
% product='nrt_global_2014';
% ctday=7;
% out_path='/home/OUTPACE/Wrk/';
%---------------------------------------------------------


%- Stations positions -% (degres east)

lat_stat = -19;
lon_stat_A = 155;
lon_stat_B = 176.32;
lon_stat_C = 200; %(°E)
SD_stat=load('../SD_stations_coord.txt');
lon_stat_SD=SD_stat(2:18,2);
lat_stat_SD=SD_stat(2:18,1);

%lvars=localvars;
if(~exist(fname,'file'))
   disp(['FILE: ',fname,' NOT FOUND!']);
	return;
end

load(fname);
% Label for figure title
titl=datestr(day0,1);

% Labels for figures
lab_fig={'OW disp','OW param','FSLE','Lon adv','Lat adv','AVISO vel','Time From Bathy'};
key_fig=['NYYNNYN'];

%%%%%%%%%%%%%%%%%%%%%%%%
%       This command disable the output to the screen, 
%       set it on 'on' if you want to see the figures before printing them!
 set (0, 'defaultfigurevisible', 'off');
 set (0,'defaultaxesfontsize',10) 
% set (0,'defaulttextfontsize',30) 
%%%%%%%%%%%%%%%%%%%%%%%%%%
%--- OW Dipsersion ----------------
nf=1; % Counter for figure and figure labels
if strcmp(key_fig(nf),'Y')==1
  figure(nf)
  clf
  imagesc(lonv,latv,OWdispersion_b,[0 60]);
  axis xy
  hold on
  h=plot(lonsmowadv60(1:4:end,1:2:end),latsmowadv60(1:4:end,1:2:end),'m.');
  set(h,'markersize',1);
  % Set axis
  %axis([lonv latv]);
  %axis([lon_zoom lat_zoom]);
  % Draw coastline
  continenta
  % Title
  title([lab_fig{nf},' ',titl])%,'fontsize',30);
  % Set colorbar
  H=colorbar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
nf=2;
if strcmp(key_fig(nf),'Y')==1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--- OW parameter -----------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 figure(nf)
 clf
 %imagesc(lonv,latv,((60*60*24).^2)*owmi,[-1 1])
 imagesc(lonv,latv,((60*60*24).^2)*owmi,[-0.3 0.3])
 hold on
 grid on;
 % Flip axis
 axis xy
 axis([lonv latv]);
 %axis([lon_zoom lat_zoom]);
xlabel('Lon','FontSize',12)
ylabel('Lat','FontSize',12)
 % Draw coastline
 continenta %LR: à modifier avec les bonnes limites de la zone
 % Title
 title([lab_fig{nf},' ',titl])%,'fontsize',30);
 % Set colorbar
 colormap(plusminusmap);
 H=colorbar; title(H,'[s^-1]')
 % Print figure
 i=nf;
 Lab_Fig=strrep(lab_fig{i},' ','_');
 figname=sprintf('%s/%d%02d%02d_%s_d%s_%s.png',out_path,day0(1),day0(2),day0(3),product,num2str(ctday-1),Lab_Fig);
 figure(i);
 %print('-depsc',figname);
 disp('%------------------')
 disp(figname)
 print(i,[figname],'-dpng');

disp(['%%%%% Figure for Station A %%%%']);
figure(nf)
clf
imagesc(lonv,latv,((60*60*24).^2)*owmi,[-0.3 0.3])
hold on
% Vector plot
sq=1;
h=quiver(lonvv(1:sq:end),latvv(1:sq:end),Ucms(1:sq:end,1:sq:end),Vcms(1:sq:end,1:sq:end),3);
set(h,'color','k');
grid on;
axis xy
axis([lon_stat_A-0.5 159 -21 -17]);
xlabel('Lon','FontSize',12)
ylabel('Lat','FontSize',12)
continenta
plot(lon_stat_A,lat_stat,'+','Color','k','Markersize',20,'linewidth',4);
text(lon_stat_A+0.1,lat_stat+0.1,'A','Color','k')%,'fontsize',30);
title([lab_fig{nf},' ',titl])%,'fontsize',30);
colormap(plusminusmap);
H=colorbar; title(H,'[s^-1]')
% Print figure
i=nf;
Lab_Fig=strrep(lab_fig{i},' ','_');
figname=sprintf('%s/%d%02d%02d_%s_d%s_%s_stationA.png',out_path,day0(1),day0(2),day0(3),product,num2str(ctday-1),Lab_Fig);
figure(i);
disp(figname)
print(i,[figname],'-dpng');

disp(['%%%%% Figure for Station B %%%%']);
figure(nf)
clf
imagesc(lonv,latv,((60*60*24).^2)*owmi,[-0.3 0.3])
hold on
% Vector plot
sq=1;
h=quiver(lonvv(1:sq:end),latvv(1:sq:end),Ucms(1:sq:end,1:sq:end),Vcms(1:sq:end,1:sq:end),3);
set(h,'color','k');
grid on;
axis xy
axis([174.2 178.5 -21 -17]);
xlabel('Lon','FontSize',12)
ylabel('Lat','FontSize',12)
continenta
plot(lon_stat_B,lat_stat,'+','Color','k','Markersize',20,'linewidth',4);
text(lon_stat_B+0.1,lat_stat+0.1,'B','Color','k','fontsize',30);
title([lab_fig{nf},' ',titl])%,'fontsize',30);
colormap(plusminusmap);
H=colorbar; title(H,'[s^-1]')
% Print figure
i=nf;
Lab_Fig=strrep(lab_fig{i},' ','_');
figname=sprintf('%s/%d%02d%02d_%s_d%s_%s_stationB.png',out_path,day0(1),day0(2),day0(3),product,num2str(ctday-1),Lab_Fig);
figure(i);
disp(figname)
print(i,[figname],'-dpng');

disp(['%%%%% Figure for Station C %%%%']);
figure(nf)
clf
imagesc(lonv,latv,((60*60*24).^2)*owmi,[-0.3 0.3])
hold on
% Vector plot
sq=1;
h=quiver(lonvv(1:sq:end),latvv(1:sq:end),Ucms(1:sq:end,1:sq:end),Vcms(1:sq:end,1:sq:end),3);
set(h,'color','k');
grid on;
axis xy
axis([196 lon_stat_C+0.5 -21 -17]);
xlabel('Lon','FontSize',12)
ylabel('Lat','FontSize',12)
continenta
plot(lon_stat_C,lat_stat,'+','Color','k','Markersize',20,'linewidth',4);
text(lon_stat_C+0.1,lat_stat+0.1,'C','Color','k','fontsize',30);
title([lab_fig{nf},' ',titl]);%,'fontsize',30);
colormap(plusminusmap);
H=colorbar; title(H,'[s^-1]')
% Print figure
i=nf;
Lab_Fig=strrep(lab_fig{i},' ','_');
figname=sprintf('%s/%d%02d%02d_%s_d%s_%s_stationC.png',out_path,day0(1),day0(2),day0(3),product,num2str(ctday-1),Lab_Fig);
figure(i);
disp(figname)
print(i,[figname],'-dpng');


close all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
nf=3;
if strcmp(key_fig(nf),'Y')==1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--- FSLE ------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 figure(nf)
 clf
 imagesc(lonv,latv,-60*60*24*lambda,[0 .2])
 hold on
 grid on;
 % Flip axis
 axis xy
 axis([lonv latv]);
 %axis([lon_zoom lat_zoom]);
 xlabel('Lon','FontSize',12)
 ylabel('Lat','FontSize',12)
 % Draw coastline
 continenta
  % Title
 title([lab_fig{nf},' ',titl]);%,'fontsize',30);
 % Set colorbar
 colormap(plusminusmap);
 H=colorbar;title(H,'[s^-1]')

 % Print figure
 i=nf;
 Lab_Fig=strrep(lab_fig{i},' ','_');
 figname=sprintf('%s/%d%02d%02d_%s_d%s_%s.png',out_path,day0(1),day0(2),day0(3),product,num2str(ctday-1),Lab_Fig);
 figure(i);
 %print('-depsc',figname);
 disp('%------------------')
 disp(figname)
 disp('%------------------')
 print(figname,'-dpng');


disp(['%%%%% Figure for Station A %%%%']);
nf=3;
figure(nf)
clf
imagesc(lonv,latv,-60*60*24*lambda,[0 0.2])
hold on
% Vector plot
sq=1;
h=quiver(lonvv(1:sq:end),latvv(1:sq:end),Ucms(1:sq:end,1:sq:end),Vcms(1:sq:end,1:sq:end),3);
set(h,'color','k');
grid on;
axis xy
xlabel('Lon','FontSize',12)
ylabel('Lat','FontSize',12)
axis([lon_stat_A-0.5 159 -21 -17]);
continenta
plot(lon_stat_A,lat_stat,'+','Color','k','Markersize',20,'linewidth',4);
text(lon_stat_A+0.1,lat_stat+0.1,'A','Color','k','fontsize',30);
title([lab_fig{nf},' ',titl])%,'fontsize',30);
colormap(plusminusmap);
H=colorbar; title(H,'[s^-1]')
% Print figure
i=nf;
Lab_Fig=strrep(lab_fig{i},' ','_');
figname=sprintf('%s/%d%02d%02d_%s_d%s_%s_stationA.png',out_path,day0(1),day0(2),day0(3),product,num2str(ctday-1),Lab_Fig);
figure(i);
disp(figname)
print(i,[figname],'-dpng');

disp(['%%%%% Figure for Station B %%%%']);
nf=3;
figure(nf)
clf
imagesc(lonv,latv,-60*60*24*lambda,[0 0.2])
hold on
% Vector plot
sq=1;
h=quiver(lonvv(1:sq:end),latvv(1:sq:end),Ucms(1:sq:end,1:sq:end),Vcms(1:sq:end,1:sq:end),3);
set(h,'color','k');
grid on;
axis xy
xlabel('Lon','FontSize',12)
ylabel('Lat','FontSize',12)
axis([174.2 178.5 -21 -17]);
continenta
plot(lon_stat_B,lat_stat,'+','Color','k','Markersize',20,'linewidth',4);
text(lon_stat_B+0.1,lat_stat+0.1,'B','Color','k','fontsize',30);
title([lab_fig{nf},' ',titl])%,'fontsize',30);
colormap(plusminusmap);
H=colorbar; title(H,'[s^-1]')
% Print figure
i=nf;
Lab_Fig=strrep(lab_fig{i},' ','_');
figname=sprintf('%s/%d%02d%02d_%s_d%s_%s_stationB.png',out_path,day0(1),day0(2),day0(3),product,num2str(ctday-1),Lab_Fig);
figure(i);
disp(figname)
print(i,[figname],'-dpng');

disp(['%%%%% Figure for Station C %%%%']);
nf=3;
figure(nf)
clf
imagesc(lonv,latv,-60*60*24*lambda,[0 0.2])
hold on
% Vector plot
sq=1;
h=quiver(lonvv(1:sq:end),latvv(1:sq:end),Ucms(1:sq:end,1:sq:end),Vcms(1:sq:end,1:sq:end),3);
set(h,'color','k');
grid on;
axis xy
xlabel('Lon','FontSize',12)
ylabel('Lat','FontSize',12)
axis([196 lon_stat_C+0.5 -21 -17]);
continenta
plot(lon_stat_C,lat_stat,'+','Color','k','Markersize',20,'linewidth',4);
text(lon_stat_C+0.1,lat_stat+0.1,'C','Color','k','fontsize',30);
title([lab_fig{nf},' ',titl])%,'fontsize',30);
colormap(plusminusmap);
H=colorbar; title(H,'[s^-1]')
% Print figure
i=nf;
Lab_Fig=strrep(lab_fig{i},' ','_');
figname=sprintf('%s/%d%02d%02d_%s_d%s_%s_stationC.png',out_path,day0(1),day0(2),day0(3),product,num2str(ctday-1),Lab_Fig);
figure(i);
disp(figname)
print(i,[figname],'-dpng');

 close all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
nf=4;
if strcmp(key_fig(nf),'Y')==1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--- Lon Advection ----------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 figure(nf)
 clf
 imagesc(lonv,latv,lonf45,lonv-5)
 hold on
 grid on;
 % flip axis
 axis xy
 axis([lonv latv]);
  xlabel('Lon','FontSize',12)
 ylabel('Lat','FontSize',12)
 %axis([lon_zoom lat_zoom]);
 % Draw coastline
 continenta
 % Draw strasse domain
 strasse_boundary
 line([Lon(1) Lon(1) Lon(2) Lon(2) Lon(1)], ...
 	[Lat(1) Lat(2) Lat(2) Lat(1) Lat(1)], ...
 	'color','m','linewidth',10)
 % Title
 title([lab_fig{nf},' ',titl])%,'fontsize',30);
 % Set colorbar
 colormap(jet);
 colorbar
 
 % Print figure
 i=nf;
 Lab_Fig=strrep(lab_fig{i},' ','_');
 figname=sprintf('%s/%d%02d%02d_%s_d%s_%s.png',out_path,day0(1),day0(2),day0(3),product,num2str(ctday-1),Lab_Fig);
 figure(i);
 %print('-depsc',figname);
 disp('%------------------')
 disp(figname)
 disp('%------------------')
 print(figname,'-dpng');
 close all
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
nf=5;
if strcmp(key_fig(nf),'Y')==1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--- Lat Advection ----------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 figure(nf)
 clf
 imagesc(lonv,latv,latf45,latv)
 hold on
 grid on;
 % Flip axis
 axis xy;
  xlabel('Lon','FontSize',12)
 ylabel('Lat','FontSize',12)
 axis([lonv latv]);
 %axis([lon_zoom lat_zoom]);
 % Draw coastline
 continenta
 % Draw strasse domain
 strasse_boundary
 line([Lon(1) Lon(1) Lon(2) Lon(2) Lon(1)], ...
 	[Lat(1) Lat(2) Lat(2) Lat(1) Lat(1)], ...
 	'color','m','linewidth',10)
 % Title
 title([lab_fig{nf},' ',titl])%,'fontsize',30);
 % Set colorbar
 colormap(jet);
 colorbar
 
 % Print figure
 i=nf;
 Lab_Fig=strrep(lab_fig{i},' ','_');
 figname=sprintf('%s/%d%02d%02d_%s_d%s_%s.png',out_path,day0(1),day0(2),day0(3),product,num2str(ctday-1),Lab_Fig);
 figure(i);
 %print('-depsc',figname);
 disp('%------------------')
 disp(figname)
 disp('%------------------')
 print(figname,'-dpng');
 
close all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
nf=6;
if strcmp(key_fig(nf),'Y')==1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--- AVISO velocities -------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(nf)
clf
imagesc(lonv,latv,sqrt(Ucms.^2+Vcms.^2),[0 100])
hold on
grid on;
% Flip axis
axis xy
 xlabel('Lon','FontSize',12)
 ylabel('Lat','FontSize',12)
 % Vector plot
sq=5;
h=quiver(lonvv(1:sq:end),latvv(1:sq:end),Ucms(1:sq:end,1:sq:end),Vcms(1:sq:end,1:sq:end),3);
set(h,'color','k');
% Draw coastline
continenta
%hi=quiver(148,-28,1,0,3);
%set(hi,'color','k');
%text(148,-29,'m/s','Fontsize',10);
% % Title
title([lab_fig{nf},' ',titl])%,'fontsize',30);
% Set colorbar
colormap(jet);
H=colorbar;title(H,'[cm s^-1]')

% Print figure
i=nf;
Lab_Fig=strrep(lab_fig{i},' ','_');
figname=sprintf('%s/%d%02d%02d_%s_d%s_%s.png',out_path,day0(1),day0(2),day0(3),product,num2str(ctday-1),Lab_Fig);
figure(i);
%print('-depsc',figname);
disp('%------------------')
disp(figname)
disp('%------------------')
print(figname,'-dpng');
close all

disp(['%%%%% Figure for Station A %%%%']);
nf=6;
figure(nf)
clf
imagesc(lonv,latv,sqrt(Ucms.^2+Vcms.^2),[0 100])
hold on
grid on;
axis xy
axis([lon_stat_A-0.5 159 -21 -17]);
 xlabel('Lon','FontSize',12)
 ylabel('Lat','FontSize',12)
 continenta
% Vector plot
sq=1;
h=quiver(lonvv(1:sq:end),latvv(1:sq:end),Ucms(1:sq:end,1:sq:end),Vcms(1:sq:end,1:sq:end),3);
set(h,'color','k');
plot(lon_stat_A,lat_stat,'+','Color','k','Markersize',20,'linewidth',4);
text(lon_stat_A+0.1,lat_stat+0.1,'A','Color','k','fontsize',30);
title([lab_fig{nf},' ',titl])%,'fontsize',30);
colormap(jet);
H=colorbar;title(H,'[cm s^-1]')
% Print figure
i=nf;
Lab_Fig=strrep(lab_fig{i},' ','_');
figname=sprintf('%s/%d%02d%02d_%s_d%s_%s_stationA.png',out_path,day0(1),day0(2),day0(3),product,num2str(ctday-1),Lab_Fig);
figure(i);
disp(figname)
print(i,[figname],'-dpng');

disp(['%%%%% Figure for Station B %%%%']);
nf=6;
figure(nf)
clf
imagesc(lonv,latv,sqrt(Ucms.^2+Vcms.^2),[0 100])
hold on
grid on;
axis xy
axis([174.2 178.5 -21 -17]);
 xlabel('Lon','FontSize',12)
 ylabel('Lat','FontSize',12)
 continenta
% Vector plot
sq=1;
h=quiver(lonvv(1:sq:end),latvv(1:sq:end),Ucms(1:sq:end,1:sq:end),Vcms(1:sq:end,1:sq:end),3);
set(h,'color','k');
plot(lon_stat_B,lat_stat,'+','Color','k','Markersize',20,'linewidth',4);
text(lon_stat_B+0.1,lat_stat+0.1,'B','Color','k','fontsize',30);
title([lab_fig{nf},' ',titl])%,'fontsize',30);
colormap(jet);
H=colorbar;title(H,'[cm s^-1]')
% Print figure
i=nf;
Lab_Fig=strrep(lab_fig{i},' ','_');
figname=sprintf('%s/%d%02d%02d_%s_d%s_%s_stationB.png',out_path,day0(1),day0(2),day0(3),product,num2str(ctday-1),Lab_Fig);
figure(i);
disp(figname)
print(i,[figname],'-dpng');

disp(['%%%%% Figure for Station C %%%%']);
nf=6;
figure(nf)
clf
imagesc(lonv,latv,sqrt(Ucms.^2+Vcms.^2),[0 100])
hold on
grid on;
axis xy
axis([196 lon_stat_C+0.5 -21 -17]);
 xlabel('Lon','FontSize',12)
 ylabel('Lat','FontSize',12)
 continenta
% Vector plot
sq=1;
h=quiver(lonvv(1:sq:end),latvv(1:sq:end),Ucms(1:sq:end,1:sq:end),Vcms(1:sq:end,1:sq:end),3);
set(h,'color','k');
plot(lon_stat_C,lat_stat,'+','Color','k','Markersize',20,'linewidth',4);
text(lon_stat_C+0.1,lat_stat+0.1,'C','Color','k','fontsize',30);
title([lab_fig{nf},' ',titl])%,'fontsize',30);
colormap(jet);
H=colorbar;title(H,'[cm s^-1]')
% Print figure
i=nf;
Lab_Fig=strrep(lab_fig{i},' ','_');
figname=sprintf('%s/%d%02d%02d_%s_d%s_%s_stationC.png',out_path,day0(1),day0(2),day0(3),product,num2str(ctday-1),Lab_Fig);
figure(i);
disp(figname)
print(i,[figname],'-dpng');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
nf=7;
if strcmp(key_fig(nf),'Y')==1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--------Time from bathy----------------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(nf)
clf
imagesc(lonv,latv,touched);axis xy
ax=axis;
hold on
continenta
%addcontpatch([.7 .7 .7]);
%addbathy('w',blvls);
colorbar
title([lab_fig{nf},' ',titl,' (day)'],'fontsize',30);
axis(ax);

% Print figure
i=nf;
Lab_Fig=strrep(lab_fig{i},' ','_');
figname=sprintf('%s/%d%02d%02d_%s_d%s_%s.png',out_path,day0(1),day0(2),day0(3),product,num2str(ctday-1),Lab_Fig);
figure(i);
%print('-depsc',figname);
disp('%------------------')
disp(figname)
disp('%------------------')
print(figname,'-dpng');
close all

%% ZOOM ON EACH STATION

%%- Stations positions -% (degres east)
%lat_stat = -19;
%lon_stat_A = 155;
%lon_stat_B = 176.32;
%lon_stat_C = 200; %(°E)
%SD_stat=load('../SD_stations_coord.txt');
%lon_stat_SD=SD_stat(2:18,2);
%lat_stat_SD=SD_stat(2:18,1);

%- Set limits of the zone around station A
        disp(['%%%%% Figure for Station A %%%%']);
        h3=figure('visible','off');
        imagesc(lonv,latv,touched);
        hold on;
        xlim([lon_stat_A-0.5 159]);
        ylim([-21 -17]);
        axis xy
	continenta
        plot(lon_stat_A,lat_stat,'+','Color','w','Markersize',20,'linewidth',4);
        text(lon_stat_A+0.1,lat_stat+0.1,'A','Color','w','fontsize',30);
        colorbar;
        title([lab_fig{nf},' ',titl,' (day)'],'fontsize',30);
%	axis(ax);
        hold off;

	Lab_Fig=strrep(lab_fig{7},' ','_');
	print(h3,[out_path,'/',num2str(day0(1)),num2str(day0(2)),num2str(day0(3)),'_',product,Lab_Fig,'_stationA.png'],'-dpng');
        disp(['     Figure  saved in ',out_path])

%- Set limits of the zone around station B
        disp(['%%%%% Figure for Station B %%%%']);
        h4=figure('visible','off');
        imagesc(lonv,latv,touched);
        hold on;
        xlim([174.2 178.5]); % Map from West Vanuatu limit (174.5°E) to 178.5°E for station B to be in the middle of the map
        ylim([-21 -17]);
        axis xy
        continenta
        plot(lon_stat_B,lat_stat,'+','Color','w','Markersize',20,'linewidth',4);
        text(lon_stat_B+0.1,lat_stat+0.1,'B','Color','w','fontsize',30);
        colorbar;
        title([lab_fig{nf},' ',titl,' (day)'],'fontsize',30);
%       axis(ax);
        hold off;

        Lab_Fig=strrep(lab_fig{7},' ','_');
        print(h4,[out_path,'/',num2str(day0(1)),num2str(day0(2)),num2str(day0(3)),'_',product,Lab_Fig,'_stationB.png'],'-dpng');

%- Set limits of the zone around station C 
        disp(['%%%%% Figure for Station C %%%%']);
        h5=figure('visible','off');
        imagesc(lonv,latv,touched);
        hold on;
        xlim([196 lon_stat_C+0.5]); % Map from 196°E to station C 
        ylim([-21 -17]);
        axis xy
        continenta
        plot(lon_stat_C,lat_stat,'+','Color','w','Markersize',20,'linewidth',4);
        text(lon_stat_C+0.1,lat_stat+0.1,'C','Color','w','fontsize',30);
        colorbar;
        title([lab_fig{nf},' ',titl,' (day)'],'fontsize',30);
%       axis(ax);
        hold off;

        Lab_Fig=strrep(lab_fig{7},' ','_');
        print(h5,[out_path,'/',num2str(day0(1)),num2str(day0(2)),num2str(day0(3)),'_',product,Lab_Fig,'_stationC.png'],'-dpng');
end
%-----------------------------------------------------------------------------
