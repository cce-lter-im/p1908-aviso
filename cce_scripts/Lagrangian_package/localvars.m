function lvars=localvars
% Previously defined in MAIN_Lagrangian.m
global DATADIR
global LAMTADIR

lvars.datadir=DATADIR;
lvars.lamtadir=LAMTADIR;
