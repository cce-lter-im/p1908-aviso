function [Uw,Vw,lonw,latw,U,V]=getncUV_upd_global_2010(nday)
% Extract u and v from delayed-time 2010 AVISO files  
% Works with AVISO files before 2014

ncnes=datenum([1950 1 1]);

dv=datevec(nday+ncnes);
day=sprintf('%d%02d%02d',dv(1),dv(2),dv(3));
lvars=localvars;
RT=6371e5;
fname=sprintf('dt_upd_global_merged_madt_uv_%s_%s_*.nc',day,day);
fnameout=sprintf('dt_upd_global_merged_madt_uv_%s_%s_%d.nc',day,day,fix(rand(1)*1000));

ncpath=sprintf('%s/aviso_upd_global_merged_madt_uv_2010',lvars.datadir);

if(~fileexists(sprintf('%s/%s.gz',ncpath,fname)))
	disp('File not found.');
	Uw=[];
	Vw=[];
	lonw=[];
	latw=[];
	return;
end

% Unzip netcdf file
cmd=sprintf('gunzip -c %s/%s.gz >%s/%s',ncpath,fname,ncpath,fnameout);
system(cmd);
ncfile=sprintf('%s/%s',ncpath,fnameout);
ncUV=netcdf(ncfile,'r');

NbLongitudes=ncUV{'NbLongitudes'}(:);
NbLatitudes=ncUV{'NbLatitudes'}(:);
U=ncUV{'Grid_0001'}(:,:);
fU=find(abs(U(:))>1e15);
U(fU)=0;
V=ncUV{'Grid_0002'}(:,:);
fV=find(abs(V(:))>1e15);
V(fV)=0;

% Create lon lat matrices
nlon=length(NbLongitudes);
nlat=length(NbLatitudes);

lon=reshape(NbLongitudes,nlon,1);
lat=reshape(NbLatitudes,nlat,1);
[glon,glat]=meshgrid(lon,lat);

% Velocities in deg/s (Used in Lyap analysis)
Ug=U./(RT.*cos(glat'./180*pi)).*180/pi;
Vg=V*180/pi./RT;

lonw=lon;
Uw=Ug;
Vw=Vg;

fw=find(lonw>180);
lonw(fw)=lonw(fw)-360;
[lonw,lonsrti]=sort(lonw);
Uw=Uw(lonsrti,:);
Vw=Vw(lonsrti,:);
Uw=Uw([(nlon-1) nlon 1:nlon 1],:);
Vw=Vw([(nlon-1) nlon 1:nlon 1],:);
lonw=lonw([(nlon-1) nlon 1:nlon 1]);
lonw(1:2)=lonw(1:2)-360;
lonw(end)=lonw(end)+360;
latw=lat;

U=U(lonsrti,:);
V=V(lonsrti,:);
U=U([(nlon-1) nlon 1:nlon 1],:);
V=V([(nlon-1) nlon 1:nlon 1],:);

% Remove unzipped copy of netcdf
cmd=sprintf('rm %s/%s',ncpath,fnameout);
system(cmd);

ncclose(ncUV);
