function ret=avisomed_nrt_2014_load(day0,dayf,ssha,withRT)
%addpath('/home/dovidio/octave/ocean');
%addpath('../../');

ret=0;

if(nargin==2)
	ssha='ssh';
	withRT=0;
	end
if(nargin==3)
	withRT=0;
	end

onedayinupd=datenum([2015 10 16])-datenum([1950 1 1]);

select(4);

[Uw,Vw,lonw,latw]=getncUV_nrt_med_2014(onedayinupd);

if(isempty(Uw))
disp('Error in finding velocity files.');
ret=2;
end



[glon,glat]=meshgrid(lonw,latw);
sz=size(Uw);

numdays=(dayf-day0)+1;

set_par([lonw(1)-360 lonw(end)-360 sz(1) latw(1) latw(end) sz(2) day0*60*60*24 dayf*60*60*24 numdays]');
print_par();
field_geometry(2,1,1);
print_par();

ctr=0;
for ctday=day0:1:dayf
[Uw,Vw,lonw,latw]=getncUV_nrt_med_2014(ctday,ssha,withRT);
if(isempty(Uw))
disp('Error in finding velocity files.');
ret=1;
return;
end

LUT_frame_fill(Uw,Vw,ctr);
ctr=ctr+1;
end

