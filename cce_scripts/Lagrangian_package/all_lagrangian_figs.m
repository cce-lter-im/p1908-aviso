function all_lagrangian_figs(fname,product,out_path,ctday)
% Create the figures using the all_lagrangian_diags .mat file
% Figures : FSLE, Okubo-Weiss parameter (OW), Lon/Lat advection, velocity
% Modified from:
% $DATA/KEOPS2/octave/keops/analysis/realtime_cron/all_lagrangianfigs_keops2.m
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 30/01/74 AD: removed function, added test option, operational mode to be checked
% 27/11/14 AD: added code to plot zooms around LD stations for all parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
warning off
%args=argv;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%if isempty(args) %%% TEST MODE %%
%%%
%  fname='/home/OUTPACE/Wrk/20150203_nrt_global_2014_d6' %%put here the name of the file produced by MAIN_Lagrangian
% product='nrt_global_2014';
%  ctday=7;
% %%%out_path='/home/OUTPACE/Figures/' AD:not ncessary already defined in the file.mat loaded%
%  addpath('/home/OUTPACE/Mfiles/')
%%%
%else %%% OPERATIONAL  MODE %%%
%%% 
disp('all_lagrangian_figs operational mode')
%end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(~exist([fname,'.mat'],'file')) 
   disp(['FILE: ',fname,'.mat NOT FOUND!']);
   return;
end

domain_limits;

load(fname)
% Label for figure title
titl=datestr(day0,1);

% Labels for figures
lab_fig={'OW disp','OW param','FSLE','Lon adv','Lat adv','AVISO vel','Time From Bathy [day]'};

%%% Choosing the figures
key_fig=['YYYYYYN'];

for ii=1:7
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nf=ii;
if strcmp(key_fig(nf),'Y')==1

h2=figure(nf,'visible','off'); hold on;


%--- OW Dispersion ----TO BE CHECKED!!!!
%if (ii==1)
%  imagesc(lonv,latv,OWdispersion_bi,[0 20]);
%end
%--- OW parameter -----------------
if (ii==2)
   imagesc(lonv,latv,((60*60*24).^2)*owmi,[-0.3 0.3])
end
%--- FSLE ------------------------
if (ii==3)
   imagesc(lonv,latv,-60*60*24*lambda,[0 .2])
end
%--- Lon Advection -----TO BE CHECKED-----------
if (ii==4)
   imagesc(lonv,latv,lonf15)
end
%--- Lat Advection -----TO BE CHECKED-----------
if (ii==5)
   imagesc(lonv,latv,latf15)
end
%--- AVISO VEL ------------------------
if (ii==6)
   imagesc(lonv,latv,sqrt(Ucms.^2+Vcms.^2),[0 40])
end
%--------Time from bathy----------------%
if (ii==7)
   imagesc(lonv,latv,touched);axis xy
end

%%% Set axis
axis xy
axis(fig_limits)
axis equal

%%% set colormap and add colorbar
if (ii==2 | ii==3)
 colormap(plusminusmap);
else
 colormap(jet);
end
 H=colorbar; 

%%% Draw coastline !!! ATTENTION: very time consuming !!!
continenta('../../Data/bathymetry/');

%%% Add station positions
stations('k')

%%% ADD grid
grid minor

%%% Title and Labels
[pathstr, fname, ext] = fileparts(fname);
day=fname(1:8);
title(lab_fig{nf});
xlabel('Longtitude');
ylabel('Latitude');

hold off
%%% Save figure
 Lab_Fig=strrep(lab_fig{ii},' ','_');
 %fileout=sprintf('%s/%d%02d%02d_%s_d%s_%s.png',out_path,day0(1),day0(2),day0(3),product,num2str(ctday-1),Lab_Fig);
 fileout=[out_path,'/',fname,'_',Lab_Fig];
% figure(i);
 %print('-depsc',figname);
 disp('%------------------')
 disp(fileout)
 print(h2,[fileout,'.png']);


% Vector plot
hold on;
sq=3;
h=quiver(lonvv(1:sq:end),latvv(1:sq:end),Ucms(1:sq:end,1:sq:end),Vcms(1:sq:end,1:sq:end),3);
set(h,'color','k');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

end%for


%%%% The End %%%%%%%%%%%%%%
