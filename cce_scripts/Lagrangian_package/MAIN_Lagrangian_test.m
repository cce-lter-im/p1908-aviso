% Make a Lagrangian analysis using AVISO uv products
%	Plot: -FSLE
%	      -Okubo-Weiss parameter
%	      -Lon and Lat advection
%	      -Velocity fields
%==============================================================================
% Modified version of:
% /mnt/netapp-nencioli/DATA/KEOPS2/octave/keops/analysis/realtime_cron/an01_kerguelen_products.m
%
% With respect to KEOPS this file is directly called from bash as opposed to
% keops_realtime_cron.bsh;
% So I added the arguments below to replace:
% - cd to lagrangian mfile directory
% - call to init.m that: - added a bunch of directories to the path
%                        - called loclavars which
%                          defined lvars.datadir and lvars.lamtadir
%                          (lvars.lamtadir is never used afterwrds)
%
% DATADIR and LAMTADIR are declared global because they are afterwards used
% inside localvars.m calle by multiple scripts;
%==============================================================================

disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp(' BEGIN OF ALL LAGRANGIAN DIAG')
disp(' ')

%-----------------------------------------------------------
% Cell array with arguments passed from bash script
 args=argv;
 
 lagrang_path=[args{1},'/'];
 global DATADIR=[args{2},'/'];
 global LAMTADIR=[args{3},'/'];
 out_path=[args{4},'/'];
%-----------------------------------------------------------

% %-----------------------------------------------------------
% % Uncomment this and comment above to test without need of bash script
% main_path='/home/ENS/apogee/r1003867/Stage_OUTPACE/OUTPACE/';
% %main_path='/media/DATA/STRASSE/';
% lagrang_path=[main_path,'Mfiles/Lagrangian_package/'];
% global DATADIR=[main_path,'Data/'];
% global LAMTADIR=[lagrang_path,'lamta.dev/'];
% out_path=[main_path,'Wrk'];
% %-----------------------------------------------------------

% To replace init.m
cd(lagrang_path)
lvars=localvars;
addpath(lvars.lamtadir)

products={'nrt_global_2014'}; % Only global products for OUTPACE
% products={'nrt_global_2010'}; % Only global products for OUTPACE

%days=[1 7]; % To process d0 and d6 data
days=[7]; % To process d0 and d6 data
% (add 4 to the array to process also d3)
 dayprod=now;
% dayprod=datenum(2014,2,11);
% dayprod=datenum(2013,2,20);

for ctproduct=1:length(products)

	product=products{ctproduct};
	%err=aviso_load(datevec(dayprod-135),datevec(floor(dayprod-1)),product);
	err=aviso_load(datevec(dayprod-2),datevec(floor(dayprod)),product);

	if(~err)
		for ctday=days
			%day0=datevec(dayprod-ctday);
			day0=datevec(dayprod);
			disp('#===============================================')
			disp(['# Processing ',product,' for d',num2str(ctday-1)])
			% Changed from all_lagrangiandiags_keops2
			fname=all_lagrangian_diags(day0,product,out_path,ctday)
			disp('%-----------------------------------------------')

			 disp(['# Figures for product ',product,' for d',num2str(ctday-1)])
			 all_lagrangian_figs(fname,product,out_path,ctday);
			 disp('#===============================================')
			 disp(' ')
		end
	end

end
