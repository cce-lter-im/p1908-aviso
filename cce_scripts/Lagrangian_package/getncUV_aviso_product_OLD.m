function [Udegs,Vdegs,lonw,latw,Ucms,Vcms,u,v,lon,lat]=getncUV_aviso_product(nday)
% [Udegs,Vdegs,lonw,latw,Ucms,Vcms,u,v,lon,lat]=getncUV_aviso_product(path_to_product,nday) reads any Aviso u,v file.
% nday is in the format: [2010 12 27]. u,v,lo,lat are the variables as in the netcdf file. Udegs, Vdegs are the currents in deg/s.
% If the product covers the longitudinally the entire globe, two latitudinal bands at 360 are repeated at O deg, in order to have trajectories over a sphere.
% Should be campatible with Matlab 

% lvars=localvars;
global DATADIR
RT=6371e5;

%From e.g. [2010 12 27] to 20101227
% LR: datenum format for date
ncnes=datenum([1950 1 1]);

dv=datevec(nday+ncnes);
ndaystr=sprintf('%d%02d%02d',dv(1),dv(2),dv(3));

% f_nencio COMMENT
% The 'products/ftp.aviso.oceanobs.com' path should not be hardcoded here !!!
% 
% fnames=glob(sprintf('%s/products/ftp.aviso.oceanobs.com/%s/%d/*_%s_*.nc.gz',lvars.datadir,path_to_product,nday(1),ndaystr));
%
% All info should be already contained in path to product
% Should we also add option if files not zipped??

%ncpath=sprintf('%s/%s/%d/*_%s_*.nc.gz',DATADIR,path_to_product,nday(1))
ncpath=sprintf('%sAVISO_uv',DATADIR);
fnames=glob(sprintf('%s/dt_global_allsat_madt_uv_%s_*.nc.gz',ncpath,ndaystr));

if(isempty(fnames))
   disp('No Aviso file found.');
   Udegs=[];,Vdegs=[];,lonw=[];,latw=[];,Ucms=[];,Vcms=[];,u=[];,v=[];,lon=[];,lat=[];
   return; 
else
   fname=fnames{end};    
   fnameout=sprintf('%s.%08d',fname,rand(1)*10000000); 
end

% f_nencio COMMENT
% The aviso file should be unzipped in ncpath (there's no tmpdir in lvars)
% 
% ncfiletmp=sprintf('%s/%s_%08d_unzipped',lvars.tmpdir,ndaystr,rand(1)*10000000);
%ncfiletmp=sprintf('%s/%s_%08d_unzipped',ncpath,ndaystr,rand(1)*10000000)

cmd=sprintf('gunzip -c %s > %s',fname,fnameout);
system(cmd);

% f_nencio COMMENT
% Different netcdf functions in new version of octave !!!!
%lon=ncread(ncfiletmp,'lon');
%lat=ncread(ncfiletmp,'lat');
%u=ncread(ncfiletmp,'u');
%v=ncread(ncfiletmp,'v');


ncfile=sprintf('%s',fnameout);
ncUV=netcdf(ncfile,'r');

lon=ncUV{'lon'}(:);
lat=ncUV{'lat'}(:);
U=ncUV{'u'}(:);
V=ncUV{'v'}(:);

% Convert 2014 aviso variables format
U=squeeze(U);
V=squeeze(V);
U=(U'); %matrix rotation
V=(V');

% Find bad values
U(U==-2147483647)=0;
V(V==-2147483647)=0;
% conversion
U=U.*0.0001; %scale factor
V=V.*0.0001;
U=U.*100;
V=V.*100; %conversion m/s to cm/s

% Create lon lat matrices
nlon=length(lon);
nlat=length(lat);

lon=reshape(lon,nlon,1);
lat=reshape(lat,nlat,1);

latw=lat;
lonw=lon;
Ucms=U;
Vcms=V;

%Finding whether lon goes all around the globe
fw=find(lonw>180);
lonw(fw)=lonw(fw)-360;
[lonw,lonsrti]=sort(lonw);
lonw=lonw([(nlon-1) nlon 1:nlon 1]);
lonw(1:2)=lonw(1:2)-360;
lonw(end)=lonw(end)+360;

if(sum(diff(diff(lonw)))<1e-9) %If the raccorded lonw is growing with constant step, lon goes around all the globe 
   Ucms=Ucms(lonsrti,:);
   Vcms=Vcms(lonsrti,:);
   Ucms=Ucms([(nlon-1) nlon 1:nlon 1],:);
   Vcms=Vcms([(nlon-1) nlon 1:nlon 1],:);
else %If the raccorded lonw has a break, then lon was regional: hence resetting lonw to lon
   lonw=lon;
end

if(max(lonw(:))>360)
	lonw=lonw-360;
end

% Velocities in deg/s
[glon,glat]=meshgrid(lonw,latw);
Udegs=Ucms./(RT.*cos(glat'./180*pi)).*180/pi;
Vdegs=Vcms*180/pi/RT;

system(sprintf('rm %s',fnameout));

ncclose(ncUV);
