#!/bin/bash
# Run this script to automate the processing of AVISO data
echo
echo '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
echo '%%%%%%  SPASSO DEVO  %%%%%%%%'
echo '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'

main_path=/home/deverneil/Desktop/SPASSO_OUTPACE/
cd $main_path
dir_data=$main_path/Data
dir_wrk=$main_path/Wrk
dir_lagrang=$main_path/Scripts/Lagrangian_package
dir_lamta=$dir_lagrang/lamta.dev/

for i in `seq 735974 736015`;
do
	echo 'About to call octave for this day: ' $i
	octave -q $dir_lagrang/MAIN_Lagrangian_dt.m $dir_lagrang $dir_data $dir_lamta $dir_wrk $i
	echo 'On this day: '$i
done
echo 'Done with this bash!'


