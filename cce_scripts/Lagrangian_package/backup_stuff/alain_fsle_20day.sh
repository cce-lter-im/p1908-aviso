#!/bin/bash
# Run this script to automate the processing of AVISO data
echo
echo '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
echo '%%%%%%  SPASSO DEVO  %%%%%%%%'
echo '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'

main_path=/home/deverneil/Desktop/AVISO_CCELTER
cd $main_path
dir_data=$main_path/Data/uv
dir_wrk=$main_path/Wrk/20Day
dir_lagrang=$main_path/CCE_Scripts/Lagrangian_package
dir_lamta=$dir_lagrang/lamta.dev

for i in `seq 735075 735091`;
do
	echo 'About to call octave for this day: ' $i
	#octave -q $dir_lagrang/MAIN_Lagrangian_nrt.m $i
	/home/deverneil/Downloads/octave-4.0.0/run-octave -q $dir_lagrang/MAIN_Lagrangian_dt20.m $dir_lagrang $dir_data $dir_lamta $dir_wrk $i
	echo 'On this day: '$i
done
echo 'Done with this bash!'


