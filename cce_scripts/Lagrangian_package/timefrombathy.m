function [touched,lons,lats,lonf,latf]=timefrombathy(day0,lons0,lats0,bathylvl,numdays,product)
%timeplateau=fromKplateau(day0,lons0,lats0,bathylvl,numdays,product)
%Gives the first day at which each particle has touched for the first time the bathymentric level bathylvl
%-1 if never touched.

[lonf,latf,lons,lats]=aviso_lonlatadvect(day0,lons0,lats0,numdays,product);

Nstep=round(abs(numdays)/(60*60*24)*8)+2;

lons=lons(1:8:end,:);%precision of 1 day
lats=lats(1:8:end,:);
sz=size(lons);
touched=zeros(size(lons0));

trajdepth=oceandepth(lons,lats);
trajdepthM=reshape(trajdepth,sz);
sz
for ct=1:length(lons0(:))
        
        [tmp,touch]=max(trajdepthM(:,ct)>bathylvl);
        if(tmp==0),touch=-1;,end
        touched(ct)=touch;
        end



