function ret=aviso_nrt_2010_load(day0,dayf,ssha)
% Load AVISO near-real-time data from 2010 netcdf format 
% Works for AVISO netcdf files before 2014
ret=0;

if(nargin==2)
	% used to distinguish if its SSH or SLA (ssa) !!!
	ssha='ssh';
end

% Test day to be loaded and retrieve values for few paramters
onedayinupd=datenum([2007 12 5])-datenum([1950 1 1]);

disp(' ')
disp('%--- Define the type of velocity field ---')
% Function in lamta.dev/lamta_all.cc which call select_sys 
% from lamta.dev/field.h
% It defines the type of velocity field (4 => user defined Lut_field)
select(4); 

% Load test day
[Uw,Vw,lonw,latw]=getncUV_upd_global_2010(onedayinupd);

if(isempty(Uw))
	disp(sprintf('Error in finding velocity files for reference day %d (upd).',onedayinupd));
	ret=2;
	return;
end

[glon,glat]=meshgrid(lonw,latw);
sz=size(Uw);

numdays=(dayf-day0)+1;

disp(' ')
disp('%--- Set parameters of LUT field --- ')
% set_par defined in lamta.dev/field.h for each field
set_par([lonw(1) lonw(end) sz(1) latw(1) latw(end) sz(2) day0*60*60*24 dayf*60*60*24 numdays]');
% No need to print_par: already in set_par !!!
%print_par();
disp(' ')

disp('%--- Set geometry of LUT field --- ')
% field_geometry(disttype,datatype,gridtype) defined in lamta.dev/lamta_all.cc
%
% disttype=1 (Euclidean), 2 (sphere).
% datatype=1 (deg./sec.), 2 (cm/sec.)
% gridtype=0 (flat), 1 (sphere, regular), 2 (sphere Mercator)
field_geometry(2,1,2);
% No need to print_par: field geometry is not displayed !!!
% print_par();
disp(' ')

disp('%--- Load velocities ---')
ctr=0;
% Cycle through each day
for ctday=day0:1:dayf
	% Read nrt global velocity field 
	[Uw,Vw,lonw,latw]=getncUV_nrt_global_2010(ctday,ssha);
	%[Uw,Vw,lonw,latw]=getncUV_rt_global(ctday);

	if(isempty(Uw))
		disp(sprintf('Error in finding nrt velocity files (day %d)',ctday));
		ret=1;
		return;
	end
	% Update the velocity field in the LUT field used
	% for the Lagrangian analysis
	disp(['% ',datestr(ctday+datenum([1950 1 1]))])
	LUT_frame_fill(Uw,Vw,ctr);
	ctr=ctr+1;
end

