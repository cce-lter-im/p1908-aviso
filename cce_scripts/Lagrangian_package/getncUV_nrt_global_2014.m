function [Uw,Vw,lonw,latw,U,V]=getncUV_nrt_global_2014(nday,ssha)
% Extract u and v from near-real-time 2014 AVISO files 

if(nargin==1)
        % used to distinguish if its SSH or SLA (ssa) !!!
        ssha='ssh';
end

ncnes=datenum([1950 1 1]);

dv=datevec(nday+ncnes);
day=sprintf('%d%02d%02d',dv(1),dv(2),dv(3));
lvars=localvars;
RT=6371e5;

% Path adjusted manually !!!
ncpath=sprintf('%s/AVISO_uv/',lvars.datadir);
fnames=glob(sprintf('%s/nrt_global_allsat_madt_uv_%s_*.nc.gz',ncpath,day));% AVISO files name have changed
if(~isempty(fnames))
        fname=fnames{end};
        fnameout=sprintf('%s.%08d',fname,rand(1)*10000000);
else
        disp('File not found.');
        Uw=[];
        Vw=[];
        lonw=[];
        latw=[];
        U=[];
        V=[];
        return;
end

% Unzip netcdf file
cmd=sprintf('gunzip -c %s >%s',fname,fnameout);
system(cmd);
ncfile=sprintf('%s',fnameout);
ncUV=netcdf(ncfile,'r');

NbLongitudes=ncUV{'lon'}(:);
NbLatitudes=ncUV{'lat'}(:);
U=ncUV{'u'}(:);
V=ncUV{'v'}(:);
% Convert 2014 aviso variables format
U=squeeze(U);
V=squeeze(V);
U=(U'); %matrix rotation
V=(V');

% Find bad values
%fU=find(abs(U(:))>1e15);
%U(fU)=0;
%fV=find(abs(V(:))>1e15);
%V(fV)=0;
U(U==-2147483647)=0;
V(V==-2147483647)=0;
% conversion
U=U.*0.0001; %scale factor
V=V.*0.0001;
U=U.*100;
V=V.*100; %conversion m/s to cm/s


% Create lon lat matrices
nlon=length(NbLongitudes);
nlat=length(NbLatitudes);

lon=reshape(NbLongitudes,nlon,1);
lat=reshape(NbLatitudes,nlat,1);
[glon,glat]=meshgrid(lon,lat);

% Velocities in deg/s (Used in Lyap analysis)
Ug=U./(RT.*cos(glat'./180*pi)).*180/pi;
Vg=V*180/pi./RT;

lonw=lon;
Uw=Ug;
Vw=Vg;


fw=find(lonw>180);
lonw(fw)=lonw(fw)-360;
[lonw,lonsrti]=sort(lonw);
Uw=Uw(lonsrti,:);
Vw=Vw(lonsrti,:);
Uw=Uw([(nlon-1) nlon 1:nlon 1],:);
Vw=Vw([(nlon-1) nlon 1:nlon 1],:);
lonw=lonw([(nlon-1) nlon 1:nlon 1]);
lonw(1:2)=lonw(1:2)-360;
lonw(end)=lonw(end)+360;
latw=lat;

U=U(lonsrti,:);
V=V(lonsrti,:);
U=U([(nlon-1) nlon 1:nlon 1],:);
V=V([(nlon-1) nlon 1:nlon 1],:);

% Case of Sea Level Anomaly
if(ssha=='ssa')
        [Ugmdt,Vgmdt,Umdt,Vmdt,glon,glat]=aviso_mdt();

        U=U-Umdt;
        V=V-Vmdt;

        Uw=Uw-Ugmdt;
        Vw=Vw-Vgmdt;
end

% Remove unzipped copy of netcdf
cmd=sprintf('rm %s',fnameout);
system(cmd);

ncclose(ncUV);
