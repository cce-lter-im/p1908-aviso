#!/usr/bin/env bash
#
#
#


PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

DATA_DIR="$PROJECT_DIR/Data/uv"
WORK_DIR="$PROJECT_DIR/Wrk"
PLOT_DIR="$PROJECT_DIR/cce_scripts/plotting"
OUTPUT_DIR="$PROJECT_DIR/Plots"
GIF_DIR="$PROJECT_DIR/GIF"

YEAR_STR=`date +%Y`
MONTH_STR=`date +%m`
DATE_STR=`date +%Y%m%d`


mkdir -p "$DATA_DIR"
mkdir -p "$WORK_DIR"


# add one day to date string formatted as YYYYMMDD
add_day () { 
    local date_str=$1
    if [ "$(uname -s)" == "Darwin" ]; then
        echo $(date -j -v +1d -f "%Y%m%d" "$date_str" +%Y%m%d)
    else
        echo $(date +%Y%m%d -d "$date_str + 1 day")
    fi        
}


# assumes you're in the directory where the file should be
file_download () {
    local host=$1
    local file=$2

    if [ -f $file ]; then
        echo "...[skipping] $file exists"
    else
        wget -N "$host/$file"
    fi

}


dload () {

    local host=http://cce.lternet.edu/2019data
    local uv_dir=alain/Data/uv
    local wk_dir=alain/Wrk

    local nrt_file=nrt_global_allsat_phy_l4_"$DATE_STR"_"$DATE_STR".nc
    local mat_file_lambda="$DATE_STR"_dt_product_d0_lambda_only.mat.gz
    local mat_file_full="$DATE_STR"_dt_product_d0.mat.gz

    cd $DATA_DIR
    echo "Getting AVISO data file for $DATE_STR..."
    file_download "$host/$uv_dir" "$nrt_file"
    gzip -d $nrt_file    

    cd $WORK_DIR
    echo "Getting .mat files for $DATE_STR"
    file_download "$host/$wk_dir" "$mat_file_full"
    gzip -d $mat_file_full

    file_download "$host/$wk_dir" "$mat_file_lambda"
    gzip -d $mat_file_lambda

    echo "Done!"
    echo 
    
    cd $PROJECT_DIR
}

upload () {
    echo "Uploading plots and GIF"
    rsync -avh --exclude=.DS_Store $OUTPUT_DIR/ vsurfweb:/var/www/html/ccelter-aliased/p1908-plots/alain/Plots/
    rsync -avh --exclude=.DS_Store $GIF_DIR/ vsurfweb:/var/www/html/ccelter-aliased/p1908-plots/alain/GIF/

    echo "Done!"
    echo
}


plot () {
    cd $PLOT_DIR

    local OUT_DIR="$OUTPUT_DIR/$DATE_STR"
    mkdir -p $OUT_DIR

    echo "Making plots"

    echo "... generating FSLE plot for $DATE_STR"
    local plot_script="plot_fsle.m"

    octave -q $plot_script $WORK_DIR $OUT_DIR $DATE_STR

    echo "... generating LAT15 plot for $DATE_STR"
    local plot_script="plot_lat15.m"

    octave -q $plot_script $WORK_DIR $OUT_DIR $DATE_STR

    echo ".. generating LON15 plot for $DATE_STR"
    local plot_script="plot_lon15.m"

    octave -q $plot_script $WORK_DIR $OUT_DIR $DATE_STR

    echo "... generating OW plot for $DATE_STR"
    local plot_script="plot_ow.m"

    octave -q $plot_script $WORK_DIR $OUT_DIR $DATE_STR

    echo "... generating SSH plot for $DATE_STR"
    local plot_script="plot_ssh.m"

    octave -q $plot_script $DATA_DIR $OUT_DIR $DATE_STR

    echo "Done"
    echo

    cd $PROJECT_DIR
}

mkgif () { 

    cd $GIF_DIR

    echo "Making GIF for last 15 days of SSH plots"

    echo "... clearing out directory"
    rm *

    local plot_dirs=$(ls $OUTPUT_DIR | sort -r)

    echo "... grabbing last 15 days of plots"
    local i=0
    for dir_date in ${plot_dirs[@]}; do
        if [ $i -eq 15 ]; then
            break
        fi

        ssh_plot="$dir_date"_SSH.png
        ssh_plot_fullpath="$OUTPUT_DIR/$dir_date/$ssh_plot"

        if [ -f $ssh_plot_fullpath ]; then
            echo "... ... copying $ssh_plot and downsizing"
            cp $ssh_plot_fullpath ./
            convert -resize 50% $ssh_plot $ssh_plot
        fi
        i=$((i + 1))
    done

    # make gif
    local gif_file="Last_15_days.gif"
    local mp4_file="Last_15_days.mp4"

    echo "... creating GIF"
    convert -delay 90 -loop 0 *.png $gif_file

    # create mp4 video
    ffmpeg -i "$gif_file" -movflags faststart -pix_fmt yuv420p -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" "$mp4_file"


    echo "... cleaning up"
    rm *png*

    echo "Done!"
    echo

    cd $PROJECT_DIR
}

# if it's past 4pm PDT, we shift the date one day forward
# to get tomorrow's data that's already available from France
TOMORROW=$(add_day "$DATE_STR")
if [ `date +%H` -gt 16 ]; then
    DATE_STR=$TOMORROW
fi


# optional date string override for regenerating specific days
if [[ "$#" -gt 1 ]]; then
    DATE_STR=$2
fi

if [ $# -lt 1 ]
then
    echo "Usage alain_aviso [dload|mkdata|plot|all]"
    exit
fi

case "$1" in

"upload")
    upload
    ;;
"mkgif")
    mkgif
    ;;
"dload")  
    dload
    ;;
"plot")
    plot
    ;;
"replot")
    echo "Deleting all existing plots and re-plotting"
    rm -rf "$OUTPUT_DIR"/*
    PROC_DATE="20190707" # re-generate all plots after this date
    while [ 1 ]
    do
        PROC_DATE=$(add_day $PROC_DATE)
        DATE_STR=$PROC_DATE
        plot
        if [ "$DATE_STR" == $(date +%Y%m%d) ]; then
            break
        fi
    done
    ;;
"all")
    dload
    plot
    mkgif
    upload
    ;;
*) 
    echo "Command $1 not recognized!"
   ;;
esac