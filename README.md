# p1980

Alain's aviso processing and plotting scripts to support the CCE LTER P1908 process cruise

# usage

Do this once a day, any time after 12am PDT

	> cd p1908-aviso
	> ./alain_aviso all

This will run all of the following commands (below), downloading the latest daily aviso file, converting this into secondary formats and running Alain's processing script to generate .mat files and then create the directory and generate the plots for the current day. 

The commands can also be run separately, **but must follow this order**:

	> cd p1908-aviso
	> ./alain_aviso dload
	> ./alain_aviso convert
	> ./alain_aviso mkdata
	> ./alain_aviso plot

By default, the `all`, `dload`, `mkdata`, and `plot` commands work using the current day's date. You can download data, generate .mat files or plot for previous days. The `convert` command always just re-converts all files in the directory, so doesn't accept a date string.

Example: generate plots (overwriting) for July 20, 2019

	> ./alain_aviso.sh plot 20190720
	
Example: downoad aviso data for July 13, 2019

	> ./alain_aviso.sh plot 20190713
	
Or just re-run a everything for a specific day

	> ./alain_aviso all 20190715
		


## running the plots individually
You can run the plots individually from the command line according to the following examples. Substitute the `20190720` for the date you want to plot

FSLE

	> cd p1908-aviso/cce_scripts/plotting
	> octave -q plot_fsle.m ../../Wrk ../../Plots 20190720

Lat 15

	> cd p1908-aviso/cce_scripts/plotting
	> octave -q plot_lat15.m ../../Wrk ../../Plots 20190720

Lon 15

	> cd p1908-aviso/cce_scripts/plotting
	> octave -q plot_lon15.m ../../Wrk ../../Plots 20190720

Okubo-Weiss

	> cd p1908-aviso/cce_scripts/plotting
	> octave -q plot_ow.m ../../Wrk ../../Plots 20190720

SSH

	> cd p1908-aviso/cce_scripts/plotting
	> octave -q plot_ssh.m ../../Data/uv ../../Plots 20190720
